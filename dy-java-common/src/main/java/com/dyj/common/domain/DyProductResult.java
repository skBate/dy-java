package com.dyj.common.domain;

/**
 * @author danmo
 * @date 2024-05-06 10:53
 **/
public class DyProductResult <T>{

    private DyProductBase base;

    private T data;

    public DyProductBase getBase() {
        return base;
    }

    public void setBase(DyProductBase base) {
        this.base = base;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
