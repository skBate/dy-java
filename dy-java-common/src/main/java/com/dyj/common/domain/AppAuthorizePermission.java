package com.dyj.common.domain;

/**
 * @author danmo
 * @date 2024-06-26 16:59
 **/
public class AppAuthorizePermission {

    /**
     * 权限类别
     */
    private String category;
    /**
     * 权限说明
     */
    private String description;
    /**
     * 权限id
     */
    private Long id;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
