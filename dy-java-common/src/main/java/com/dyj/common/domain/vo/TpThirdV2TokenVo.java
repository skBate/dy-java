package com.dyj.common.domain.vo;

/**
 * @author danmo
 * @date 2024-06-26 16:32
 **/
public class TpThirdV2TokenVo {

    /**
     * 第三方小程序应用接口调用凭据
     */
    private String component_access_token;
    /**
     * 有效期，单位：秒
     */
    private Long expires_in;
    /**
     * 错误码, 只有当错误时返回
     */
    private String errno;
    /**
     * 错误描述, 只有当错误时返回
     */
    private String message;

    public String getComponent_access_token() {
        return component_access_token;
    }

    public void setComponent_access_token(String component_access_token) {
        this.component_access_token = component_access_token;
    }

    public Long getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(Long expires_in) {
        this.expires_in = expires_in;
    }

    public String getErrno() {
        return errno;
    }

    public void setErrno(String errno) {
        this.errno = errno;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
