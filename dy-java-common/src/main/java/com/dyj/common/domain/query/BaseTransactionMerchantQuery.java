package com.dyj.common.domain.query;

import com.dyj.common.enums.TransactionMerchantTokenTypeEnum;

/**
 * 基本交易进件接口请求
 */
public class BaseTransactionMerchantQuery extends BaseQuery{

    /**
     * 交易系统进件接口accessToken类型 默认为非服务商身份获取token
     */
    private TransactionMerchantTokenTypeEnum transactionMerchantTokenType = TransactionMerchantTokenTypeEnum.CLIENT_TOKEN;

    public TransactionMerchantTokenTypeEnum getTransactionMerchantTokenType() {
        return transactionMerchantTokenType;
    }

    public BaseTransactionMerchantQuery setTransactionMerchantTokenType(TransactionMerchantTokenTypeEnum transactionMerchantTokenType) {
        this.transactionMerchantTokenType = transactionMerchantTokenType;
        return this;
    }
}
