package com.dyj.common.config;

/**
 * @author danmo
 * @date 2024-04-02 14:02
 **/
public class AgentConfiguration {

    /**
     * 租户ID
     */
    private Integer tenantId;
    /**
     * 应用Key
     */
    private String clientKey;

    /**
     * 应用秘钥
     */
    private String clientSecret;

    /**
     * 三方应用回调ticket
     */
    private String ticket;

    /**
     * 三方应用授权码
     */
    private String authorization_code;


    public Integer getTenantId() {
        return tenantId;
    }

    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    public String getClientKey() {
        return clientKey;
    }

    public void setClientKey(String clientKey) {
        this.clientKey = clientKey;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getAuthorization_code() {
        return authorization_code;
    }

    public void setAuthorization_code(String authorization_code) {
        this.authorization_code = authorization_code;
    }
}
