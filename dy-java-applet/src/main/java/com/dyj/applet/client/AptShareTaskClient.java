package com.dyj.applet.client;

import com.dtflys.forest.annotation.BaseRequest;
import com.dtflys.forest.annotation.JSONBody;
import com.dtflys.forest.annotation.Post;
import com.dtflys.forest.annotation.Var;
import com.dyj.applet.domain.query.CreateUserTaskQuery;
import com.dyj.applet.domain.vo.CreateTaskVo;
import com.dyj.applet.domain.vo.UserShareTaskVo;
import com.dyj.applet.domain.vo.UserTaskVo;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.BaseQuery;
import com.dyj.common.interceptor.ClientTokenInterceptor;

import java.util.List;

/**
 * 分享
 * 拍抖音任务
 * 拍抖音互动任务
 * 分享任务
 *
 * @author danmo
 * @date 2024-05-27 18:29
 **/
@BaseRequest(baseURL = "${domain}")
public interface AptShareTaskClient {

    /**
     * 创建用户任务
     *
     * @param query 入参
     * @return DySimpleResult<CreateTaskVo>
     */
    @Post(url = "/api/apps/v1/douyin/create_task/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<CreateTaskVo> createUserTask(@JSONBody CreateUserTaskQuery query);

    /**
     * 查询用户任务进度
     *
     * @param query  应用信息
     * @param appId  小程序ID
     * @param openId 开启任务的用户openID
     * @param taskId 任务 ID 列表
     * @return DySimpleResult<UserTaskVo>
     */
    @Post(url = "/api/apps/v1/douyin/query_user_task/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<UserTaskVo> queryUserTask(@Var("query") BaseQuery query, @JSONBody("app_id") String appId, @JSONBody("open_id") String openId, @JSONBody("task_id") List<String> taskId);

    /**
     * 创建互动任务
     *
     * @param query 入参
     * @return DySimpleResult<CreateTaskVo>
     */
    @Post(url = "/api/apps/v1/douyin/create_interact_task/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<CreateTaskVo> createInteractTask(@JSONBody CreateUserTaskQuery query);

    /**
     * 查询互动任务进度
     *
     * @param query  应用信息
     * @param appId  小程序ID
     * @param openId 开启任务的用户openID
     * @param taskId 任务 ID 列表
     * @return DySimpleResult<UserTaskVo>
     */
    @Post(url = "/api/apps/v1/douyin/query_user_interact_task/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<UserTaskVo> queryUserInteractTask(@Var("query") BaseQuery query, @JSONBody("app_id") String appId, @JSONBody("open_id") String openId, @JSONBody("task_id") List<String> taskId);

    /**
     * 创建分享任务
     *
     * @param query       应用信息入参
     * @param targetCount 目标分享次数，分享给非重复用户算作成功分享
     * @param taskType    任务类型 1：分享任务
     * @param startTime   开始时间戳，秒级
     * @param endTime     结束时间戳，秒级，任务持续时间最长一年
     * @return
     */
    @Post(url = "/api/apps/v1/share/create_task/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<CreateTaskVo> createShareTask(@Var("query") BaseQuery query, @JSONBody("target_count") Long targetCount, @JSONBody("task_type") Integer taskType, @JSONBody("start_time") Long startTime, @JSONBody("end_time") Long endTime);

    /**
     * 查询分享任务进度
     *
     * @param query  应用信息入参
     * @param openId 开启任务的用户openid
     * @param taskId 任务id
     * @return DySimpleResult<UserShareTaskVo>
     */
    @Post(url = "/api/apps/v1/share/query_user_task/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<UserShareTaskVo> queryUserShareTask(@Var("query") BaseQuery query, @JSONBody("open_id") String openId, @JSONBody("task_id") List<String> taskId);
}
