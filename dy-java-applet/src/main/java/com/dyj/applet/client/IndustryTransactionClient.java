package com.dyj.applet.client;

import com.dtflys.forest.annotation.BaseRequest;
import com.dtflys.forest.annotation.JSONBody;
import com.dtflys.forest.annotation.Post;
import com.dyj.applet.domain.query.QueryIndustryOrderCpsQuery;
import com.dyj.applet.domain.query.QueryIndustryOrderQuery;
import com.dyj.applet.domain.query.QueryIndustryItemOrderInfoQuery;
import com.dyj.applet.domain.vo.QueryIndustryCpsVo;
import com.dyj.applet.domain.vo.QueryIndustryOrderVo;
import com.dyj.applet.domain.vo.QueryIndustryItemOrderInfoVo;
import com.dyj.common.domain.DataAndExtraVo;
import com.dyj.common.interceptor.ClientTokenInterceptor;

/**
 * 交易系统 生活服务交易系统
 */
@BaseRequest(baseURL = "${domain}")
public interface IndustryTransactionClient {


    /**
     * 查询订单基本信息。
     * @param body 查询订单基本信息。请求值
     * @return
     */
    @Post(value = "${queryIndustryOrder}", interceptor = ClientTokenInterceptor.class)
    DataAndExtraVo<QueryIndustryOrderVo> queryIndustryOrder(@JSONBody QueryIndustryOrderQuery body);

    /**
     * 查询券状态信息
     * @param body 查询券状态信息请求值
     * @return
     */
    @Post(value = "${queryIndustryItemOrderInfo}", interceptor = ClientTokenInterceptor.class)
    DataAndExtraVo<QueryIndustryItemOrderInfoVo> queryIndustryItemOrderInfo(@JSONBody QueryIndustryItemOrderInfoQuery body);

    /**
     * 生活服务交易系统->查询 CPS 信息
     * @param body 查询 CPS 信息请求值
     * @return
     */
    @Post(value = "${queryIndustryOrderCps}", interceptor = ClientTokenInterceptor.class)
    DataAndExtraVo<QueryIndustryCpsVo> queryIndustryOrderCps(@JSONBody QueryIndustryOrderCpsQuery body);
}
