package com.dyj.applet.client;

import com.dtflys.forest.annotation.*;
import com.dtflys.forest.backend.ContentType;
import com.dyj.applet.domain.*;
import com.dyj.applet.domain.query.*;
import com.dyj.applet.domain.vo.*;
import com.dyj.common.domain.DataAndExtraVo;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.BaseTransactionMerchantQuery;
import com.dyj.common.interceptor.ClientTokenInterceptor;
import com.dyj.common.interceptor.TransactionMerchantTokenInterceptor;

import java.io.InputStream;
import java.util.List;

/**
 * 交易系统 通用交易系统
 */
@BaseRequest(baseURL = "${domain}")
public interface TransactionClient {

    /**
     * 进件图片上传
     * @param imageType 图片格式，支持格式：jpg、jpeg、png
     * @param imageContent 图片二进制字节流，最大为2M
     * @return
     */
    @Post(value = "${merchantImageUpload}", interceptor = TransactionMerchantTokenInterceptor.class,contentType = ContentType.MULTIPART_FORM_DATA)
    DySimpleResult<ImageUploadImageIdVo> merchantImageUpload(@Var("query") BaseTransactionMerchantQuery query, @Body(name = "image_type") String imageType, @DataFile(value = "image_content",fileName = "image_content") InputStream imageContent);


    /**
     * 发起进件
     * @param body 发起进件请求值
     * @return
     */
    @Post(value = "${createSubMerchantV3}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<CreateSubMerchantDataVo> createSubMerchantV3(@JSONBody CreateSubMerchantMerchantQuery body);

    /**
     * 进件查询
     * @param body 进件查询请求值
     * @return
     */
    @Post(value = "${queryMerchantStatusV3}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<QueryMerchantStatus> queryMerchantStatusV3(@JSONBody QueryMerchantStatusMerchantQuery body);

    /**
     * 开发者获取小程序收款商户/合作方进件页面
     * @param body 开发者获取小程序收款商户/合作方进件页面请求值
     * @return
     */
    @Post(value = "${openAppAddSubMerchant}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<MerchantUrl> openAppAddSubMerchant(@JSONBody GetMerchantUrlMerchantQuery body);

    /**
     * 服务商获取小程序收款商户进件页面
     * @param body 服务商获取小程序收款商户进件页面请求值
     * @return
     */
    @Post(value = "${openAddAppMerchant}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<MerchantUrl> openAddAppMerchant(@JSONBody GetMerchantUrlMerchantQuery body);

    /**
     * 服务商获取服务商进件页面
     * @param body 服务商获取服务商进件页面请求值
     * @return
     */
    @Post(value = "${openSaasAddMerchant}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<MerchantUrl> openSaasAddMerchant(@JSONBody OpenSaasAddMerchantMerchantQuery body);

    /**
     * 服务商获取合作方进件页面
     * @param body 服务商获取合作方进件页面请求值
     * @return
     */
    @Post(value = "${openSaasAddSubMerchant}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<MerchantUrl> openSaasAddSubMerchant(@JSONBody OpenSaasAddSubMerchantMerchantQuery body);

    /**
     * 查询标签组信息
     * @param body 查询标签组信息请求值
     * @return
     */
    @Post(value = "${tagQuery}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<TagQueryVo> tagQuery(@JSONBody TagQueryQuery body);

    /**
     * 查询CPS信息
     * @param body 查询CPS信息请求值
     * @return
     */
    @Post(value = "${queryCpsOpenApi}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<QueryTransactionCps> queryCps(@JSONBody QueryTransactionCpsQuery body);

    /**
     * 查询订单信息
     * @param body 查询订单信息请求值
     * @return
     */
    @Post(value = "${queryOrderOpenApi}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<QueryTransactionOrder> queryOrder(@JSONBody QueryTransactionOrderQuery body);

    /**
     * 发起退款
     * @param body 发起退款请求值
     * @return
     */
    @Post(value = "${createRefundOpenApi}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<CreateRefundVo> createRefund(@JSONBody CreateRefundQuery body);

    /**
     * 查询退款
     * @param body 查询退款请求值
     * @return
     */
    @Post(value = "${queryRefundOpenApi}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<QueryRefundResultVo> queryRefund(@JSONBody QueryRefundQuery body);

    /**
     * 同步退款审核结果
     * @param body 同步退款审核结果请求值
     * @return
     */
    @Post(value = "${merchantAuditCallback}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<?> merchantAuditCallback(@JSONBody MerchantAuditCallbackQuery body);

    /**
     * 推送履约状态
     * @param body 推送履约状态请求值
     * @return
     */
    @Post(value = "${fulfillPushStatus}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<?> fulfillPushStatus(@JSONBody FulfillPushStatusQuery body);

    /**
     * 发起分账
     * @param body 发起分账请求值
     * @return
     */
    @Post(value = "${createSettle}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<CreateSettleVo> createSettle(@JSONBody CreateSettleQuery body);

    /**
     * 查询分账
     * @param body 查询分账请求值
     * @return
     */
    @Post(value = "${querySettle}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<List<QuerySettleResult>> querySettle(@JSONBody QuerySettleQuery body);

    /**
     * 商户余额查询
     * @param body 商户余额查询请求值
     * @return
     */
    @Post(value = "${openQueryChannelBalanceAccount}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<QueryChannelBalanceAccountVo> queryChannelBalanceAccount(@JSONBody QueryChannelBalanceAccountQuery body);

    /**
     * 商户提现
     * @param body 商户提现请求值
     * @return
     */
    @Post(value = "${openMerchantWithdraw}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<MerchantWithdrawVo> merchantWithdraw(@JSONBody MerchantWithdrawQuery body);

    /**
     * 商户提现结果查询
     * @param body 商户提现结果查询请求值
     * @return
     */
    @Post(value = "${openQueryWithdrawOrder}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<QueryWithdrawOrderVo> queryWithdrawOrder(@JSONBody QueryWithdrawOrderQuery body);

    /**
     * 开发者获取小程序收款商户/合作方提现页面
     * @param body
     * @return
     */
    @Post(value = "${saasAppAddSubMerchant}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<MerchantUrl> saasAppAddSubMerchant(@JSONBody SaasAppAddSubMerchantQuery body);

    /**
     * 服务商获取小程序收款商户提现页面
     * @param body
     * @return
     */
    @Post(value = "${saasGetAppMerchant}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<MerchantUrl> saasGetAppMerchant(@JSONBody OpenAddAppMerchantMerchantQuery body);

    /**
     * 服务商获取服务商提现页面
     * @param body
     * @return
     */
    @Post(value = "${saasAddMerchant}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<MerchantUrl> saasAddMerchant(@JSONBody SaasAddMerchantQuery body);

    /**
     * 服务商获取合作方提现页面
     * @param body
     * @return
     */
    @Post(value = "${saasAddSubMerchant}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<MerchantUrl> saasAddSubMerchant(@JSONBody OpenSaasAddSubMerchantMerchantQuery body);

    /**
     * 获取资金账单
     * @param body 获取资金账单请求值
     * @return
     */
    @Post(value = "${getFundBill}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<GetFundBillVo> getFundBill(@JSONBody GetFundBillQuery body);

    /**
     * 获取交易账单
     * @param body 获取交易账单请求值
     * @return
     */
    @Post(value = "${getBill}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<GetBillVo> getBill(@JSONBody GetBillQuery body);

}
