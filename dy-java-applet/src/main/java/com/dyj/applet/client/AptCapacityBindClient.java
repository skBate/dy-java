package com.dyj.applet.client;

import com.dtflys.forest.annotation.*;
import com.dyj.applet.domain.query.BindAwemeRelaionQuery;
import com.dyj.applet.domain.vo.AwemeBindTemplateInfoVo;
import com.dyj.applet.domain.vo.AwemeBindTemplateListVo;
import com.dyj.applet.domain.vo.AwemeRelationBindQrcodeVo;
import com.dyj.applet.domain.vo.AwemeRelationListVo;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.BaseQuery;
import com.dyj.common.interceptor.ClientTokenInterceptor;
import com.dyj.common.interceptor.TpV2AuthTokenHeaderInterceptor;

import java.util.List;

/**
 * 抖音号绑定
 *
 * @author danmo
 * @date 2024-06-24 16:18
 **/
@BaseRequest(baseURL = "${domain}", contentType = "application/json")
public interface AptCapacityBindClient {


    /**
     * 获取抖音号绑定所需的资质模版列表
     *
     * @param query        应用信息
     * @param awemeId      抖音号
     * @param type         绑定类型
     *                     brand：品牌号绑定
     *                     cooperation：合作号绑定
     *                     employee：员工号绑定
     * @param capacityList 绑定能力列表
     *                     video_self_mount：短视频自主挂载
     *                     live_self_mount：直播自主挂载
     *                     ma.im.life_im: 本地生活
     *                     imma.component.instant_messaging：私信组件
     *                     ma.jsapi.authorizePrivateMessage：主动授权私信组件
     * @return DySimpleResult<AwemeBindTemplateInfoVo>
     */
    @Post(value = "/api/apps/v1/capacity/get_aweme_bind_template_list/", interceptor = TpV2AuthTokenHeaderInterceptor.class)
    DySimpleResult<AwemeBindTemplateListVo> getAwemeBindTemplateList(@Var("query") BaseQuery query, @JSONBody("aweme_id") String awemeId, @JSONBody("type") String type, @JSONBody("capacity_list") List<Long> capacityList);

    /**
     * 获取抖音号绑定所需的资质模版信息
     *
     * @param query      应用信息
     * @param awemeId    抖音号
     * @param type       绑定类型  brand：品牌号绑定  cooperation：合作号绑定  employee：员工号绑定
     * @param templateId 资质模版ID列表，通过获取抖音号绑定所需的资质模版列表接口获取
     * @return DySimpleResult<AwemeBindTemplateInfoVo>
     */
    @Post(value = "/api/apps/v1/capacity/get_aweme_bind_template_info/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AwemeBindTemplateInfoVo> getAwemeBindTemplateInfo(@Var("query") BaseQuery query, @JSONBody("aweme_id") String awemeId, @JSONBody("type") String type, @JSONBody("template_id") List<Long> templateId);

    /**
     * 输入抖音号绑定
     *
     * @param query 请求参数
     * @return DySimpleResult<String>
     */
    @Post(value = "/api/apps/v1/capacity/bind_aweme_relation/", interceptor = TpV2AuthTokenHeaderInterceptor.class)
    DySimpleResult<String> bindAwemeRelation(@JSONBody BindAwemeRelaionQuery query);

    /**
     * 获取抖音号绑定二维码
     *
     * @param query        应用信息
     * @param capacityList 绑定能力列表
     *                     video_self_mount：短视频自主挂载
     *                     live_self_mount：直播自主挂载
     *                     ma.im.life_im: 本地生活im
     * @param type         绑定类型，当前仅品牌号支持获取绑定二维码   brand：品牌号绑定
     * @param coSubject    抖音号和小程序是否是相同的主体，品牌号绑定时必传
     * @return
     */
    @Get(value = "/api/apps/v1/capacity/get_aweme_relation_bind_qrcode/", interceptor = TpV2AuthTokenHeaderInterceptor.class)
    DySimpleResult<AwemeRelationBindQrcodeVo> getAwemeRelationBindQrcode(@Var("query") BaseQuery query, @Query("capacity_list") List<String> capacityList, @Query("type") String type, @Query("co_subject") Boolean coSubject);

    /**
     * 查询抖音号绑定列表及状态
     *
     * @param query    应用信息
     * @param pageNum  分页编号，从1开始
     * @param pageSize 分页大小
     * @param type     绑定类型 brand：品牌号绑定 cooperation：合作号绑定 employee：员工号绑定
     * @return DySimpleResult<AwemeRelationListVo>
     */
    @Get(value = "/api/apps/v1/capacity/query_aweme_relation_list/", interceptor = TpV2AuthTokenHeaderInterceptor.class)
    DySimpleResult<AwemeRelationListVo> queryAwemeRelationList(@Var("query") BaseQuery query, @Query("page_num") Long pageNum, @Query("page_size") Long pageSize, @Query("type") String type);

    /**
     * 解除抖音号绑定
     *
     * @param query   应用信息
     * @param awemeId 抖音号
     * @param type    绑定类型brand：品牌号绑定cooperation：合作号绑定employee：员工号绑定
     * @return DySimpleResult<String>
     */
    @Post(value = "/api/apps/v1/capacity/unbind_aweme_relation/", interceptor = TpV2AuthTokenHeaderInterceptor.class)
    DySimpleResult<String> unbindAwemeRelation(@Var("query") BaseQuery query, @JSONBody("aweme_id") String awemeId, @JSONBody("type") String type);
}
