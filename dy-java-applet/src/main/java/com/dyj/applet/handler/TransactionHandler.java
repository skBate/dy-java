package com.dyj.applet.handler;

import com.dyj.applet.domain.*;
import com.dyj.applet.domain.query.*;
import com.dyj.applet.domain.vo.*;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DataAndExtraVo;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.BaseTransactionMerchantQuery;

import java.io.InputStream;
import java.util.List;

/**
 * 交易系统 通用交易系统
 */
public class TransactionHandler extends AbstractAppletHandler{
    public TransactionHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }

    /**
     * 进件图片上传
     * @param imageType 图片格式，支持格式：jpg、jpeg、png
     * @param imageContent 图片二进制字节流，最大为2M
     * @return
     */
    public DySimpleResult<ImageUploadImageIdVo> merchantImageUpload(String imageType, InputStream imageContent){
        BaseTransactionMerchantQuery baseQuery = new BaseTransactionMerchantQuery();
        baseQuery.setClientKey(agentConfiguration.getClientKey());
        baseQuery.setTenantId(agentConfiguration.getTenantId());
        return getTransactionClient().merchantImageUpload(baseQuery, imageType, imageContent);
    }

    /**
     * 发起进件
     * @param body 发起进件请求值
     * @return
     */
    public DySimpleResult<CreateSubMerchantDataVo> createSubMerchantV3(CreateSubMerchantMerchantQuery body){
        baseQuery(body);
        return getTransactionClient().createSubMerchantV3(body);
    }

    /**
     * 进件查询
     * @param body 进件查询请求值
     * @return
     */
    public DySimpleResult<QueryMerchantStatus> queryMerchantStatusV3(QueryMerchantStatusMerchantQuery body){
        baseQuery(body);
        return getTransactionClient().queryMerchantStatusV3(body);
    }

    /**
     * 开发者获取小程序收款商户/合作方进件页面
     * @param body 开发者获取小程序收款商户/合作方进件页面请求值
     * @return
     */
    public DySimpleResult<MerchantUrl> openAppAddSubMerchant(GetMerchantUrlMerchantQuery body){
        baseQuery(body);
        return getTransactionClient().openAppAddSubMerchant(body);
    }

    /**
     * 服务商获取小程序收款商户进件页面
     * @param body 服务商获取小程序收款商户进件页面请求值
     * @return
     */
    public DySimpleResult<MerchantUrl> openAddAppMerchant(GetMerchantUrlMerchantQuery body){
        baseQuery(body);
        return getTransactionClient().openAddAppMerchant(body);
    }

    /**
     * 服务商获取服务商进件页面
     * @param body 服务商获取服务商进件页面请求值
     * @return
     */
    public DySimpleResult<MerchantUrl> openSaasAddMerchant(OpenSaasAddMerchantMerchantQuery body){
        baseQuery(body);
        return getTransactionClient().openSaasAddMerchant(body);
    }

    /**
     * 服务商获取合作方进件页面
     * @param body 服务商获取合作方进件页面请求值
     * @return
     */
    public DySimpleResult<MerchantUrl> openSaasAddSubMerchant(OpenSaasAddSubMerchantMerchantQuery body) {
        baseQuery(body);
        return getTransactionClient().openSaasAddSubMerchant(body);
    }

    /**
     * 查询标签组信息
     * @param body 查询标签组信息请求值
     * @return
     */
    public DySimpleResult<TagQueryVo> tagQuery(TagQueryQuery body){
        baseQuery(body);
        return getTransactionClient().tagQuery(body);
    }

    /**
     * 查询CPS信息
     * @param body 查询CPS信息请求值
     * @return
     */
    public DySimpleResult<QueryTransactionCps> queryCps(QueryTransactionCpsQuery body){
        baseQuery(body);
        return getTransactionClient().queryCps(body);
    }

    /**
     * 查询订单信息
     * @param body 查询订单信息请求值
     * @return
     */
    public DySimpleResult<QueryTransactionOrder> queryOrder(QueryTransactionOrderQuery body) {
        baseQuery(body);
        return getTransactionClient().queryOrder(body);
    }

    /**
     * 发起退款
     * @param body 发起退款请求值
     * @return
     */
    public DySimpleResult<CreateRefundVo> createRefund(CreateRefundQuery body) {
        baseQuery(body);
        return getTransactionClient().createRefund(body);
    }

    /**
     * 查询退款
     * @param body 查询退款请求值
     * @return
     */
    public DySimpleResult<QueryRefundResultVo> queryRefund(QueryRefundQuery body) {
        baseQuery(body);
        return getTransactionClient().queryRefund(body);
    }

    /**
     * 同步退款审核结果
     * @param body 同步退款审核结果请求值
     * @return
     */
    public DySimpleResult<?> merchantAuditCallback(MerchantAuditCallbackQuery body) {
        baseQuery(body);
        return getTransactionClient().merchantAuditCallback(body);
    }

    /**
     * 推送履约状态
     * @param body 推送履约状态请求值
     * @return
     */
    public DySimpleResult<?> fulfillPushStatus(FulfillPushStatusQuery body){
        baseQuery(body);
        return getTransactionClient().fulfillPushStatus(body);
    }

    /**
     * 发起分账
     * @param body 发起分账请求值
     * @return
     */
    public DySimpleResult<CreateSettleVo> createSettle(CreateSettleQuery body) {
        baseQuery(body);
        return getTransactionClient().createSettle(body);
    }

    /**
     * 查询分账
     * @param body 查询分账请求值
     * @return
     */
    public DySimpleResult<List<QuerySettleResult>> querySettle(QuerySettleQuery body) {
        baseQuery(body);
        return getTransactionClient().querySettle(body);
    }

    /**
     * 商户余额查询
     * @param body 商户余额查询请求值
     * @return
     */
    public DySimpleResult<QueryChannelBalanceAccountVo> queryChannelBalanceAccount(QueryChannelBalanceAccountQuery body){
        baseQuery(body);
        return getTransactionClient().queryChannelBalanceAccount(body);
    }

    /**
     * 商户提现
     * @param body 商户提现请求值
     * @return
     */
    public DySimpleResult<MerchantWithdrawVo> merchantWithdraw(MerchantWithdrawQuery body){
        baseQuery(body);
        return getTransactionClient().merchantWithdraw(body);
    }

    /**
     * 商户提现结果查询
     * @param body 商户提现结果查询请求值
     * @return
     */
    public DySimpleResult<QueryWithdrawOrderVo> queryWithdrawOrder(QueryWithdrawOrderQuery body) {
        baseQuery(body);
        return getTransactionClient().queryWithdrawOrder(body);
    }

    /**
     * 开发者获取小程序收款商户/合作方提现页面
     * @param body
     * @return
     */
    public DySimpleResult<MerchantUrl> saasAppAddSubMerchant(SaasAppAddSubMerchantQuery body){
        baseQuery(body);
        return getTransactionClient().saasAppAddSubMerchant(body);
    }

    /**
     * 服务商获取小程序收款商户提现页面
     * @param body
     * @return
     */
    public DySimpleResult<MerchantUrl> saasGetAppMerchant(OpenAddAppMerchantMerchantQuery body){
        baseQuery(body);
        return getTransactionClient().saasGetAppMerchant(body);
    }

    /**
     * 服务商获取服务商提现页面
     * @param body
     * @return
     */
    public DySimpleResult<MerchantUrl> saasAddMerchant(SaasAddMerchantQuery body){
        baseQuery(body);
        return getTransactionClient().saasAddMerchant(body);
    }

    /**
     * 服务商获取合作方提现页面
     * @param body
     * @return
     */
    public DySimpleResult<MerchantUrl> saasAddSubMerchant(OpenSaasAddSubMerchantMerchantQuery body) {
        baseQuery(body);
        return getTransactionClient().saasAddSubMerchant(body);
    }

    /**
     * 获取资金账单
     * @param body 获取资金账单请求值
     * @return
     */
    public DySimpleResult<GetFundBillVo> getFundBill(GetFundBillQuery body) {
        baseQuery(body);
        return getTransactionClient().getFundBill(body);
    }

    /**
     * 获取交易账单
     * @param body 获取交易账单请求值
     * @return
     */
    public DySimpleResult<GetBillVo> getBill(GetBillQuery body) {
        baseQuery(body);
        return getTransactionClient().getBill(body);
    }
}
