package com.dyj.applet.handler;

import com.dtflys.forest.annotation.JSONBody;
import com.dyj.applet.domain.query.QueryIndustryOrderCpsQuery;
import com.dyj.applet.domain.query.QueryIndustryOrderQuery;
import com.dyj.applet.domain.query.QueryIndustryItemOrderInfoQuery;
import com.dyj.applet.domain.vo.QueryIndustryCpsVo;
import com.dyj.applet.domain.vo.QueryIndustryOrderVo;
import com.dyj.applet.domain.vo.QueryIndustryItemOrderInfoVo;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DataAndExtraVo;

/**
 * 交易系统 生活服务交易系统
 */
public class IndustryTransactionHandler extends AbstractAppletHandler{
    public IndustryTransactionHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }


    /**
     * 查询订单基本信息。
     * @param body 查询订单基本信息。请求值
     * @return
     */
    public DataAndExtraVo<QueryIndustryOrderVo> queryIndustryOrder(QueryIndustryOrderQuery body){
        baseQuery(body);
        return getIndustryOpenTransactionClient().queryIndustryOrder(body);
    }

    /**
     * 查询券状态信息
     * @param body 查询券状态信息请求值
     * @return
     */
    public DataAndExtraVo<QueryIndustryItemOrderInfoVo> queryIndustryItemOrderInfo(QueryIndustryItemOrderInfoQuery body){
        baseQuery(body);
        return getIndustryOpenTransactionClient().queryIndustryItemOrderInfo(body);
    }

    /**
     * 生活服务交易系统->查询 CPS 信息
     * @param body 查询 CPS 信息请求值
     * @return
     */
    public DataAndExtraVo<QueryIndustryCpsVo> queryIndustryOrderCps(QueryIndustryOrderCpsQuery body){
        baseQuery(body);
        return getIndustryOpenTransactionClient().queryIndustryOrderCps(body);
    }
}
