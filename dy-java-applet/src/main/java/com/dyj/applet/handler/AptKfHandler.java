package com.dyj.applet.handler;

import com.dtflys.forest.annotation.Query;
import com.dtflys.forest.annotation.Var;
import com.dyj.applet.domain.vo.AptKfUrlVo;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.UserInfoQuery;

/**
 * @author danmo
 * @date 2024-06-06 09:54
 **/
public class AptKfHandler extends AbstractAppletHandler{

    public AptKfHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }

    /**
     * 获取客服链接
     * @param appId 授权小程序 appid
     * @param openId 用户在当前小程序的 ID，使用 code2session 接口返回的 openid
     * @param type 来源，抖音传 1128，抖音极速版传 2329
     * @param scene 场景值，固定值 1
     * @param orderId  订单号
     * @param imType im_type声明客服所属行业，小程序服务类目必须属于该行业 "group_buy"：酒旅、美食、其他本地服务行业
     * @return DySimpleResult<AptKfUrlVo>
     */
    public DySimpleResult<AptKfUrlVo> getKfUrl(String appId, String openId, String type, String scene,String orderId, String imType){
        return getKfClient().getKfUrl(userInfoQuery(openId), appId, openId, type, scene, orderId, imType);
    }
}
