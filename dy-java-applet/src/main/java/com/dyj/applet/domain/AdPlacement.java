package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-06-17 15:27
 **/
public class AdPlacement {

    /**
     * 广告位id
     */
    private String ad_placement_id;
    /**
     * 广告位名称
     */
    private String ad_placement_name;
    /**
     * 广告位类型
     * 1：Banner广告
     * 2：激励视频广告
     * 3：信息流广告
     * 4：插屏广告
     * 5：视频前贴片广告
     * 6：视频后贴片广告
     */
    private String ad_placement_type;

    /**
     * 广告位状态
     * 0：关闭
     * 1：开启
     */
    private Integer status;


    public String getAd_placement_id() {
        return ad_placement_id;
    }

    public void setAd_placement_id(String ad_placement_id) {
        this.ad_placement_id = ad_placement_id;
    }

    public String getAd_placement_name() {
        return ad_placement_name;
    }

    public void setAd_placement_name(String ad_placement_name) {
        this.ad_placement_name = ad_placement_name;
    }

    public String getAd_placement_type() {
        return ad_placement_type;
    }

    public void setAd_placement_type(String ad_placement_type) {
        this.ad_placement_type = ad_placement_type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
