package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseTransactionMerchantQuery;
import com.dyj.common.enums.TransactionMerchantTokenTypeEnum;

/**
 * 获取进件页面请求值
 */
public class GetMerchantUrlMerchantQuery extends BaseTransactionMerchantQuery {

    /**
     * 小程序id
     */
    private String app_id;
    /**
     * <p><strong>1</strong>：收款商户（可以收款也可以接收分账）</p><p><strong>2</strong>：商户的合作方（只可以接收分账）</p><p>不传默认为2</p> 选填
     */
    private Integer role;
    /**
     * <p>商户 id，用于接入方自行标识并管理进件方。由开发者自行分配管理</p>
     */
    private String sub_merchant_id;
    /**
     * <p>链接类型枚举值：</p><p>1: 进件页面</p> 选填
     */
    private Integer url_type;

    public String getApp_id() {
        return app_id;
    }

    public GetMerchantUrlMerchantQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public Integer getRole() {
        return role;
    }

    public GetMerchantUrlMerchantQuery setRole(Integer role) {
        this.role = role;
        return this;
    }

    public String getSub_merchant_id() {
        return sub_merchant_id;
    }

    public GetMerchantUrlMerchantQuery setSub_merchant_id(String sub_merchant_id) {
        this.sub_merchant_id = sub_merchant_id;
        return this;
    }

    public Integer getUrl_type() {
        return url_type;
    }

    public GetMerchantUrlMerchantQuery setUrl_type(Integer url_type) {
        this.url_type = url_type;
        return this;
    }

    public static GetMerchantUrlQueryBuilder builder() {
        return new GetMerchantUrlQueryBuilder();
    }

    public static final class GetMerchantUrlQueryBuilder {
        private String app_id;
        private Integer role;
        private String sub_merchant_id;
        private Integer url_type;
        private TransactionMerchantTokenTypeEnum transactionMerchantTokenType = TransactionMerchantTokenTypeEnum.CLIENT_TOKEN;
        private Integer tenantId;
        private String clientKey;

        private GetMerchantUrlQueryBuilder() {
        }

        public GetMerchantUrlQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public GetMerchantUrlQueryBuilder role(Integer role) {
            this.role = role;
            return this;
        }

        public GetMerchantUrlQueryBuilder subMerchantId(String subMerchantId) {
            this.sub_merchant_id = subMerchantId;
            return this;
        }

        public GetMerchantUrlQueryBuilder urlType(Integer urlType) {
            this.url_type = urlType;
            return this;
        }

        public GetMerchantUrlQueryBuilder transactionMerchantTokenType(TransactionMerchantTokenTypeEnum transactionMerchantTokenType) {
            this.transactionMerchantTokenType = transactionMerchantTokenType;
            return this;
        }

        public GetMerchantUrlQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public GetMerchantUrlQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public GetMerchantUrlMerchantQuery build() {
            GetMerchantUrlMerchantQuery getMerchantUrlQuery = new GetMerchantUrlMerchantQuery();
            getMerchantUrlQuery.setApp_id(app_id);
            getMerchantUrlQuery.setRole(role);
            getMerchantUrlQuery.setSub_merchant_id(sub_merchant_id);
            getMerchantUrlQuery.setUrl_type(url_type);
            getMerchantUrlQuery.setTransactionMerchantTokenType(transactionMerchantTokenType);
            getMerchantUrlQuery.setTenantId(tenantId);
            getMerchantUrlQuery.setClientKey(clientKey);
            return getMerchantUrlQuery;
        }
    }
}
