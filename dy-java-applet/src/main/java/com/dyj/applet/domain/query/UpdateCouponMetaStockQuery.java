package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 修改券模板库存请求值
 */
public class UpdateCouponMetaStockQuery extends BaseQuery {


    /**
     * <p>增减动作（增加：increase；减少暂不支持）</p>
     */
    private String action;
    /**
     * 小程序appid
     */
    private String app_id;
    /**
     * <p>抖音开平券模板id，<strong>券模板的创建接口会返回该字段</strong></p>
     */
    private String coupon_meta_id;
    /**
     * <p>增减动作对应的数量（需大于0）</p>
     */
    private Long number;
    /**
     * <p>开发者唯一幂等键，<strong>用于抖音开平侧幂等处理</strong></p>
     */
    private String unique_key;

    public String getAction() {
        return action;
    }

    public UpdateCouponMetaStockQuery setAction(String action) {
        this.action = action;
        return this;
    }

    public String getApp_id() {
        return app_id;
    }

    public UpdateCouponMetaStockQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getCoupon_meta_id() {
        return coupon_meta_id;
    }

    public UpdateCouponMetaStockQuery setCoupon_meta_id(String coupon_meta_id) {
        this.coupon_meta_id = coupon_meta_id;
        return this;
    }

    public Long getNumber() {
        return number;
    }

    public UpdateCouponMetaStockQuery setNumber(Long number) {
        this.number = number;
        return this;
    }

    public String getUnique_key() {
        return unique_key;
    }

    public UpdateCouponMetaStockQuery setUnique_key(String unique_key) {
        this.unique_key = unique_key;
        return this;
    }

    public static UpdateCouponMetaStockQueryBuilder builder(){
        return new UpdateCouponMetaStockQueryBuilder();
    }

    public static final class UpdateCouponMetaStockQueryBuilder {
        private String action;
        private String app_id;
        private String coupon_meta_id;
        private Long number;
        private String unique_key;
        private Integer tenantId;
        private String clientKey;

        private UpdateCouponMetaStockQueryBuilder() {
        }

        public static UpdateCouponMetaStockQueryBuilder anUpdateCouponMetaStockQuery() {
            return new UpdateCouponMetaStockQueryBuilder();
        }

        public UpdateCouponMetaStockQueryBuilder action(String action) {
            this.action = action;
            return this;
        }

        public UpdateCouponMetaStockQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public UpdateCouponMetaStockQueryBuilder couponMetaId(String couponMetaId) {
            this.coupon_meta_id = couponMetaId;
            return this;
        }

        public UpdateCouponMetaStockQueryBuilder number(Long number) {
            this.number = number;
            return this;
        }

        public UpdateCouponMetaStockQueryBuilder uniqueKey(String uniqueKey) {
            this.unique_key = uniqueKey;
            return this;
        }

        public UpdateCouponMetaStockQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public UpdateCouponMetaStockQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public UpdateCouponMetaStockQuery build() {
            UpdateCouponMetaStockQuery updateCouponMetaStockQuery = new UpdateCouponMetaStockQuery();
            updateCouponMetaStockQuery.setAction(action);
            updateCouponMetaStockQuery.setApp_id(app_id);
            updateCouponMetaStockQuery.setCoupon_meta_id(coupon_meta_id);
            updateCouponMetaStockQuery.setNumber(number);
            updateCouponMetaStockQuery.setUnique_key(unique_key);
            updateCouponMetaStockQuery.setTenantId(tenantId);
            updateCouponMetaStockQuery.setClientKey(clientKey);
            return updateCouponMetaStockQuery;
        }
    }
}
