package com.dyj.applet.domain.query;

import com.dyj.applet.domain.CouponAvailableInfo;
import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-06 16:52
 **/
public class SyncV2CouponAvailableQuery extends BaseQuery {

    /**
     * 可用列表
     */
    private List<CouponAvailableInfo> available_info;

    /**
     * 外部优惠券ID
     */
    private String ext_coupon_id;

    /**
     * 同步行为 1-上线，2-下线
     */
    private Integer status;

    public static SyncV2CouponAvailableQueryBuilder builder() {
        return new SyncV2CouponAvailableQueryBuilder();
    }

    public static class SyncV2CouponAvailableQueryBuilder {
        private List<CouponAvailableInfo> availableInfo;
        private String extCouponId;
        private Integer status;
        private Integer tanantId;
        private String clientKey;

        public SyncV2CouponAvailableQueryBuilder availableInfo(List<CouponAvailableInfo> availableInfo) {
            this.availableInfo = availableInfo;
            return this;
        }

        public SyncV2CouponAvailableQueryBuilder extCouponId(String extCouponId) {
            this.extCouponId = extCouponId;
            return this;
        }

        public SyncV2CouponAvailableQueryBuilder status(Integer status) {
            this.status = status;
            return this;
        }

        public SyncV2CouponAvailableQueryBuilder tanantId(Integer tanantId) {
            this.tanantId = tanantId;
            return this;
        }

        public SyncV2CouponAvailableQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public SyncV2CouponAvailableQuery build() {
            SyncV2CouponAvailableQuery syncV2CouponAvailableQuery = new SyncV2CouponAvailableQuery();
            syncV2CouponAvailableQuery.setAvailable_info(availableInfo);
            syncV2CouponAvailableQuery.setExt_coupon_id(extCouponId);
            syncV2CouponAvailableQuery.setStatus(status);
            syncV2CouponAvailableQuery.setTenantId(tanantId);
            syncV2CouponAvailableQuery.setClientKey(clientKey);
            return syncV2CouponAvailableQuery;
        }
    }

    public List<CouponAvailableInfo> getAvailable_info() {
        return available_info;
    }

    public void setAvailable_info(List<CouponAvailableInfo> available_info) {
        this.available_info = available_info;
    }

    public String getExt_coupon_id() {
        return ext_coupon_id;
    }

    public void setExt_coupon_id(String ext_coupon_id) {
        this.ext_coupon_id = ext_coupon_id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
