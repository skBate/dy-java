package com.dyj.applet.domain;

import java.util.List;

/**
 * @author danmo
 * @date 2024-06-24 16:45
 **/
public class EmployeeInfo {

    /**
     * 员工身份证号
     */
    private String identity_number;

    /**
     * 员工姓名
     */
    private String real_name;

    /**
     * 关系过期时间，时间戳最长一年有效期
     */
    private Long relation_expire_date;

    /**
     * 劳动合同照片，使用上传素材接口获取到的路径上传素材接口type传2
     * contract_image和qualification_image_list必传一个
     */
    private String contract_image;

    /**
     * 资质材料列表，使用上传素材接口获取到的路径上传素材接口type传2
     * contract_image和qualification_image_list必传一个
     */
    private List<String> qualification_image_list;

    /**
     * 资质类型
     */
    private String qualification_type;

    public static class EmployeeInfoBuilder {

        /**
         * 员工身份证号
         */
        private String identityNumber;

        /**
         * 员工姓名
         */
        private String realName;

        /**
         * 关系过期时间，时间戳最长一年有效期
         */
        private Long relationExpireDate;

        /**
         * 劳动合同照片，使用上传素材接口获取到的路径上传素材接口type传2
         * contract_image和qualification_image_list必传一个
         */
        private String contractImage;

        /**
         * 资质材料列表，使用上传素材接口获取到的路径上传素材接口type传2
         * contract_image和qualification_image_list必传一个
         */
        private List<String> qualificationImageList;

        /**
         * 资质类型
         */
        private String qualificationType;

        public EmployeeInfoBuilder identityNumber(String identityNumber) {
            this.identityNumber = identityNumber;
            return this;
        }

        public EmployeeInfoBuilder realName(String realName) {
            this.realName = realName;
            return this;
        }

        public EmployeeInfoBuilder relationExpireDate(Long relationExpireDate) {
            this.relationExpireDate = relationExpireDate;
            return this;
        }

        public EmployeeInfoBuilder contractImage(String contractImage) {
            this.contractImage = contractImage;
            return this;
        }

        public EmployeeInfoBuilder qualificationImageList(List<String> qualificationImageList) {
            this.qualificationImageList = qualificationImageList;
            return this;
        }

        public EmployeeInfoBuilder qualificationType(String qualificationType) {
            this.qualificationType = qualificationType;
            return this;
        }

        public EmployeeInfo build() {
            EmployeeInfo employeeInfo = new EmployeeInfo();
            employeeInfo.setIdentity_number(identityNumber);
            employeeInfo.setReal_name(realName);
            employeeInfo.setRelation_expire_date(relationExpireDate);
            employeeInfo.setContract_image(contractImage);
            employeeInfo.setQualification_image_list(qualificationImageList);
            employeeInfo.setQualification_type(qualificationType);
            return employeeInfo;
        }
    }

    public static EmployeeInfoBuilder builder() {
        return new EmployeeInfoBuilder();
    }

    public String getIdentity_number() {
        return identity_number;
    }

    public void setIdentity_number(String identity_number) {
        this.identity_number = identity_number;
    }

    public String getReal_name() {
        return real_name;
    }

    public void setReal_name(String real_name) {
        this.real_name = real_name;
    }

    public Long getRelation_expire_date() {
        return relation_expire_date;
    }

    public void setRelation_expire_date(Long relation_expire_date) {
        this.relation_expire_date = relation_expire_date;
    }

    public String getContract_image() {
        return contract_image;
    }

    public void setContract_image(String contract_image) {
        this.contract_image = contract_image;
    }

    public List<String> getQualification_image_list() {
        return qualification_image_list;
    }

    public void setQualification_image_list(List<String> qualification_image_list) {
        this.qualification_image_list = qualification_image_list;
    }

    public String getQualification_type() {
        return qualification_type;
    }

    public void setQualification_type(String qualification_type) {
        this.qualification_type = qualification_type;
    }
}
