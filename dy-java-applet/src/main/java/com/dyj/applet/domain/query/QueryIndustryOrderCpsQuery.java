package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 生活服务交易系统查询 CPS 信息请求值
 */
public class QueryIndustryOrderCpsQuery extends BaseQuery {

    /**
     * 抖音开平内部交易订单号，该单号通过预下单回调传给开发者服务，长度 < 64byte。 选填
     */
    private String order_id;
    /**
     * 开发者系统生成的订单号，与唯一 order_id 关联，长度 < 64byte。 选填
     */
    private String out_order_no;

    public String getOrder_id() {
        return order_id;
    }

    public QueryIndustryOrderCpsQuery setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getOut_order_no() {
        return out_order_no;
    }

    public QueryIndustryOrderCpsQuery setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public static QueryIndustryOrderCpsQueryBuilder builder(){
        return new QueryIndustryOrderCpsQueryBuilder();
    }


    public static final class QueryIndustryOrderCpsQueryBuilder {
        private String order_id;
        private String out_order_no;
        private Integer tenantId;
        private String clientKey;

        private QueryIndustryOrderCpsQueryBuilder() {
        }

        public QueryIndustryOrderCpsQueryBuilder orderId(String orderId) {
            this.order_id = orderId;
            return this;
        }

        public QueryIndustryOrderCpsQueryBuilder outOrderNo(String outOrderNo) {
            this.out_order_no = outOrderNo;
            return this;
        }

        public QueryIndustryOrderCpsQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QueryIndustryOrderCpsQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QueryIndustryOrderCpsQuery build() {
            QueryIndustryOrderCpsQuery queryIndustryOrderCpsQuery = new QueryIndustryOrderCpsQuery();
            queryIndustryOrderCpsQuery.setOrder_id(order_id);
            queryIndustryOrderCpsQuery.setOut_order_no(out_order_no);
            queryIndustryOrderCpsQuery.setTenantId(tenantId);
            queryIndustryOrderCpsQuery.setClientKey(clientKey);
            return queryIndustryOrderCpsQuery;
        }
    }
}
