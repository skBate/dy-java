package com.dyj.applet.domain.vo;

import com.dyj.common.domain.vo.BaseVo;

/**
 * @author danmo
 * @date 2024-05-06 14:29
 **/
public class SyncOrderVo extends BaseVo {

    private String order_id;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }
}
