package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AuditTemplateInfo;

import java.util.Map;

/**
 * @author danmo
 * @date 2024-06-24 17:06
 **/
public class AwemeBindTemplateInfoVo {

    /**
     * 资质模版mapmap的key为资质模版ID，value为模版详细信息
     */
    private Map<String, AuditTemplateInfo> template_info;

    public Map<String, AuditTemplateInfo> getTemplate_info() {
        return template_info;
    }

    public void setTemplate_info(Map<String, AuditTemplateInfo> template_info) {
        this.template_info = template_info;
    }
}
