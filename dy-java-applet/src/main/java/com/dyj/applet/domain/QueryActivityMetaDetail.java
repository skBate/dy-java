package com.dyj.applet.domain;

/**
 * 查询授权用户发放的活动信息
 */
public class QueryActivityMetaDetail {


    /**
     * 活动id
     */
    private String activity_id;
    /**
     * 活动状态
     */
    private Integer activity_status;
    /**
     * 小程序icon
     */
    private String app_icon;
    /**
     * 小程序id
     */
    private String app_id;
    /**
     * 小程序名称
     */
    private String app_name;
    /**
     * 使用说明
     */
    private String consume_desc;
    /**
     * 使用时间说明
     */
    private String consume_time_desc;
    /**
     * 券图片
     */
    private String coupon_icon;
    /**
     * 券模板id
     */
    private String coupon_meta_id;
    /**
     * 券名称
     */
    private String coupon_meta_name;
    /**
     * 优惠金额，单位分，仅DiscountType是满减、立减时会赋值 选填
     */
    private Long discount_amount;
    /**
     * 券类型
     */
    private Integer discount_type;
    /**
     * 券类型，中文名
     */
    private String discount_type_name;
    /**
     * 用户可领取的开始时间，秒级时间戳
     */
    private Long receive_begin_time;
    /**
     * 用户可领取的结束时间，秒级时间戳
     */
    private Long receive_end_time;
    /**
     * 剩余库存
     */
    private Long remain_stock;
    /**
     * 分享裂变说明，如：不支持分享、分享被领取后奖励1张XX券
     */
    private String share_fission_desc;
    /**
     * 是否支持分享裂变
     */
    private Boolean support_share_fission;
    /**
     * 总库存
     */
    private Long total_stock;

    public String getActivity_id() {
        return activity_id;
    }

    public QueryActivityMetaDetail setActivity_id(String activity_id) {
        this.activity_id = activity_id;
        return this;
    }

    public Integer getActivity_status() {
        return activity_status;
    }

    public QueryActivityMetaDetail setActivity_status(Integer activity_status) {
        this.activity_status = activity_status;
        return this;
    }

    public String getApp_icon() {
        return app_icon;
    }

    public QueryActivityMetaDetail setApp_icon(String app_icon) {
        this.app_icon = app_icon;
        return this;
    }

    public String getApp_id() {
        return app_id;
    }

    public QueryActivityMetaDetail setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getApp_name() {
        return app_name;
    }

    public QueryActivityMetaDetail setApp_name(String app_name) {
        this.app_name = app_name;
        return this;
    }

    public String getConsume_desc() {
        return consume_desc;
    }

    public QueryActivityMetaDetail setConsume_desc(String consume_desc) {
        this.consume_desc = consume_desc;
        return this;
    }

    public String getConsume_time_desc() {
        return consume_time_desc;
    }

    public QueryActivityMetaDetail setConsume_time_desc(String consume_time_desc) {
        this.consume_time_desc = consume_time_desc;
        return this;
    }

    public String getCoupon_icon() {
        return coupon_icon;
    }

    public QueryActivityMetaDetail setCoupon_icon(String coupon_icon) {
        this.coupon_icon = coupon_icon;
        return this;
    }

    public String getCoupon_meta_id() {
        return coupon_meta_id;
    }

    public QueryActivityMetaDetail setCoupon_meta_id(String coupon_meta_id) {
        this.coupon_meta_id = coupon_meta_id;
        return this;
    }

    public String getCoupon_meta_name() {
        return coupon_meta_name;
    }

    public QueryActivityMetaDetail setCoupon_meta_name(String coupon_meta_name) {
        this.coupon_meta_name = coupon_meta_name;
        return this;
    }

    public Long getDiscount_amount() {
        return discount_amount;
    }

    public QueryActivityMetaDetail setDiscount_amount(Long discount_amount) {
        this.discount_amount = discount_amount;
        return this;
    }

    public Integer getDiscount_type() {
        return discount_type;
    }

    public QueryActivityMetaDetail setDiscount_type(Integer discount_type) {
        this.discount_type = discount_type;
        return this;
    }

    public String getDiscount_type_name() {
        return discount_type_name;
    }

    public QueryActivityMetaDetail setDiscount_type_name(String discount_type_name) {
        this.discount_type_name = discount_type_name;
        return this;
    }

    public Long getReceive_begin_time() {
        return receive_begin_time;
    }

    public QueryActivityMetaDetail setReceive_begin_time(Long receive_begin_time) {
        this.receive_begin_time = receive_begin_time;
        return this;
    }

    public Long getReceive_end_time() {
        return receive_end_time;
    }

    public QueryActivityMetaDetail setReceive_end_time(Long receive_end_time) {
        this.receive_end_time = receive_end_time;
        return this;
    }

    public Long getRemain_stock() {
        return remain_stock;
    }

    public QueryActivityMetaDetail setRemain_stock(Long remain_stock) {
        this.remain_stock = remain_stock;
        return this;
    }

    public String getShare_fission_desc() {
        return share_fission_desc;
    }

    public QueryActivityMetaDetail setShare_fission_desc(String share_fission_desc) {
        this.share_fission_desc = share_fission_desc;
        return this;
    }

    public Boolean getSupport_share_fission() {
        return support_share_fission;
    }

    public QueryActivityMetaDetail setSupport_share_fission(Boolean support_share_fission) {
        this.support_share_fission = support_share_fission;
        return this;
    }

    public Long getTotal_stock() {
        return total_stock;
    }

    public QueryActivityMetaDetail setTotal_stock(Long total_stock) {
        this.total_stock = total_stock;
        return this;
    }
}
