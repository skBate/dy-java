package com.dyj.applet.domain;

/**
 * 订单核销详细信息
 */
public class QueryIndustryCpsDeliveryInfoDeliveryItem {

    /**
     * 核销金额，单位分
     */
    private Long delivery_amount;
    /**
     * 核销时间，13 位毫秒时间戳
     */
    private Long delivery_at;
    /**
     * 显示核销状态，默认“已核销”
     */
    private String delivery_status;
    /**
     * 抖音开平侧的商品单号，只存在交易系统中
     */
    private String item_order_id;

    public Long getDelivery_amount() {
        return delivery_amount;
    }

    public QueryIndustryCpsDeliveryInfoDeliveryItem setDelivery_amount(Long delivery_amount) {
        this.delivery_amount = delivery_amount;
        return this;
    }

    public Long getDelivery_at() {
        return delivery_at;
    }

    public QueryIndustryCpsDeliveryInfoDeliveryItem setDelivery_at(Long delivery_at) {
        this.delivery_at = delivery_at;
        return this;
    }

    public String getDelivery_status() {
        return delivery_status;
    }

    public QueryIndustryCpsDeliveryInfoDeliveryItem setDelivery_status(String delivery_status) {
        this.delivery_status = delivery_status;
        return this;
    }

    public String getItem_order_id() {
        return item_order_id;
    }

    public QueryIndustryCpsDeliveryInfoDeliveryItem setItem_order_id(String item_order_id) {
        this.item_order_id = item_order_id;
        return this;
    }
}
