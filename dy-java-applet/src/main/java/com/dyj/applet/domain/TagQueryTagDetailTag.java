package com.dyj.applet.domain;

import java.util.List;

/**
 * 标签组对应标签信息
 */
public class TagQueryTagDetailTag {

    /**
     * <p>行业类型，枚举值：</p><p>2001：通信行业</p><p>2002：咨询行业</p><p>2003：内容消费行业</p><p>2004：工具行业</p>
     */
    private Integer biz_line;
    /**
     * <p>商品类型，枚举值:</p><p>101: "号卡商品",</p><p>102: "通信定制类商品(彩铃)",</p><p>103: "话费/宽带充值类商品"</p><p>201: "通用咨询类商品"</p><p>202:"代写文书"</p><p>301: "虚拟工具类商品"</p><p>401: "内容消费类商品"</p>
     */
    private Integer goods_type;
    /**
     * <p>标签id</p>
     */
    private String tag_id;
    /**
     * <p>标签简介</p>
     */
    private String tag_name;
    /**
     * <p>标签类型，枚举值：</p><p>1：退款标签</p>
     */
    private Integer tag_type;
    /**
     * <p>标签描述</p>
     */
    private List<String> tag_descs;

    public Integer getBiz_line() {
        return biz_line;
    }

    public TagQueryTagDetailTag setBiz_line(Integer biz_line) {
        this.biz_line = biz_line;
        return this;
    }

    public Integer getGoods_type() {
        return goods_type;
    }

    public TagQueryTagDetailTag setGoods_type(Integer goods_type) {
        this.goods_type = goods_type;
        return this;
    }

    public String getTag_id() {
        return tag_id;
    }

    public TagQueryTagDetailTag setTag_id(String tag_id) {
        this.tag_id = tag_id;
        return this;
    }

    public String getTag_name() {
        return tag_name;
    }

    public TagQueryTagDetailTag setTag_name(String tag_name) {
        this.tag_name = tag_name;
        return this;
    }

    public Integer getTag_type() {
        return tag_type;
    }

    public TagQueryTagDetailTag setTag_type(Integer tag_type) {
        this.tag_type = tag_type;
        return this;
    }

    public List<String> getTag_descs() {
        return tag_descs;
    }

    public TagQueryTagDetailTag setTag_descs(List<String> tag_descs) {
        this.tag_descs = tag_descs;
        return this;
    }
}
