package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.ChannelBalanceAccountInfo;
import com.dyj.applet.domain.ChannelBalanceAccountSettleInfo;

/**
 * 查询余额信息返回值
 */
public class QueryChannelBalanceAccountVo {

    /**
     * <p>抖音信息和光合信号标识：</p><p>1: 当前余额所属抖音信息</p><p>2: 当前余额所属光合信号</p>
     */
    private Integer merchant_entity;
    /**
     * <p>余额信息</p>
     */
    private ChannelBalanceAccountInfo account_info;
    /**
     * <p>结算信息</p>
     */
    private ChannelBalanceAccountSettleInfo settle_info;

    public Integer getMerchant_entity() {
        return merchant_entity;
    }

    public QueryChannelBalanceAccountVo setMerchant_entity(Integer merchant_entity) {
        this.merchant_entity = merchant_entity;
        return this;
    }

    public ChannelBalanceAccountInfo getAccount_info() {
        return account_info;
    }

    public QueryChannelBalanceAccountVo setAccount_info(ChannelBalanceAccountInfo account_info) {
        this.account_info = account_info;
        return this;
    }

    public ChannelBalanceAccountSettleInfo getSettle_info() {
        return settle_info;
    }

    public QueryChannelBalanceAccountVo setSettle_info(ChannelBalanceAccountSettleInfo settle_info) {
        this.settle_info = settle_info;
        return this;
    }
}
