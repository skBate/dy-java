package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AdSettlement;

import java.util.List;

/**
 * @author danmo
 * @date 2024-06-24 16:10
 **/
public class AdSettlementVo {

    private List<AdSettlement> settlement_list;

    public List<AdSettlement> getSettlement_list() {
        return settlement_list;
    }

    public void setSettlement_list(List<AdSettlement> settlement_list) {
        this.settlement_list = settlement_list;
    }
}
