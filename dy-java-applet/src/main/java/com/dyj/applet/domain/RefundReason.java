package com.dyj.applet.domain;

/**
 * 退款理由
 */
public class RefundReason {

    /**
     * <p>退款原因code，必须从以下code中选择:[{"code":101,"text":"不想要了"},{"code":102,"text":"商家服务原因"},{"code":103,"text":"商品质量问题"},{"code":999,"text":"其他"}]</p>
     */
    private Long code;
    /**
     * <p>退款原因描述，开发者可自定义，长度&lt;50</p>
     */
    private String text;

    public Long getCode() {
        return code;
    }

    public RefundReason setCode(Long code) {
        this.code = code;
        return this;
    }

    public String getText() {
        return text;
    }

    public RefundReason setText(String text) {
        this.text = text;
        return this;
    }
}
