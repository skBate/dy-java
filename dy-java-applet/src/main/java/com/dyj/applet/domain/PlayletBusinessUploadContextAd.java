package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-21 10:25
 **/
public class PlayletBusinessUploadContextAd {

    /**
     * 广告click_id/callback。如用户来自广告，则开发者需要回传本参数
     */
    private String callback;

    public PlayletBusinessUploadContextAd(String callback) {
        this.callback = callback;
    }

    public String getCallback() {
        return callback;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }
}
