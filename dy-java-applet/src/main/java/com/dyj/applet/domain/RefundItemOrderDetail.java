package com.dyj.applet.domain;

/**
 * 退款订单商品信息
 */
public class RefundItemOrderDetail {
    /**
     * <p>商品单号，参见通用参数-重要 ID 字段说明</p>
     */
    private String item_order_id;
    /**
     * <p>该item_order需要退款的金额，单位[分]，必须&gt;0且不能大于该 item_order 实付金额</p>
     */
    private Long refund_amount;

    public String getItem_order_id() {
        return item_order_id;
    }

    public RefundItemOrderDetail setItem_order_id(String item_order_id) {
        this.item_order_id = item_order_id;
        return this;
    }

    public Long getRefund_amount() {
        return refund_amount;
    }

    public RefundItemOrderDetail setRefund_amount(Long refund_amount) {
        this.refund_amount = refund_amount;
        return this;
    }
}
