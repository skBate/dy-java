package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AptUserComment;
import com.dyj.common.domain.vo.BaseVo;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-24 14:14
 **/
public class AptUserCommentVo extends BaseVo {

    private List<AptUserComment> result_list;

    public List<AptUserComment> getResult_list() {
        return result_list;
    }

    public void setResult_list(List<AptUserComment> result_list) {
        this.result_list = result_list;
    }
}
