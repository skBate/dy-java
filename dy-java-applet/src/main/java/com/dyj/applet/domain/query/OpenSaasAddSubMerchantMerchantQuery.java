package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseTransactionMerchantQuery;
import com.dyj.common.enums.TransactionMerchantTokenTypeEnum;

/**
 * 服务商获取合作方进件页面请求值
 */
public class OpenSaasAddSubMerchantMerchantQuery extends BaseTransactionMerchantQuery {


    /**
     * <p>需要进件的支付解决方案</p><p>1：<a href="https://developer.open-douyin.com/docs/resource/zh-CN/mini-app/open-capacity/guaranteed-payment/guide/merchant" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">担保支付｜普通版</a></p><p>2：<a href="https://developer.open-douyin.com/docs/resource/zh-CN/mini-app/open-capacity/guaranteed-payment/guide/merchant-E" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">担保支付｜企业版</a></p><p>不传默认为1</p><p>注意：在同一种支付解决方案下的商户号才可以互相分账</p> 选填
     */
    private Integer prod_id;
    /**
     * <p>prod_id=2的情况下使用</p><p>1：收款商户（可以收款也可以接收分账）</p><p>2：商户的合作方（只可以接收分账）</p><p>不传默认为2</p> 选填
     */
    private Integer role;
    /**
     * <p>商户 id，用于接入方自行标识并管理进件方。由开发者自行分配管理</p>
     */
    private String sub_merchant_id;
    /**
     * <p>小程序第三方平台应用 id</p>
     */
    private String thirdparty_id;
    /**
     * <p>链接类型枚举值：1: 进件页面</p>
     */
    private Integer url_type;

    public Integer getProd_id() {
        return prod_id;
    }

    public OpenSaasAddSubMerchantMerchantQuery setProd_id(Integer prod_id) {
        this.prod_id = prod_id;
        return this;
    }

    public Integer getRole() {
        return role;
    }

    public OpenSaasAddSubMerchantMerchantQuery setRole(Integer role) {
        this.role = role;
        return this;
    }

    public String getSub_merchant_id() {
        return sub_merchant_id;
    }

    public OpenSaasAddSubMerchantMerchantQuery setSub_merchant_id(String sub_merchant_id) {
        this.sub_merchant_id = sub_merchant_id;
        return this;
    }

    public String getThirdparty_id() {
        return thirdparty_id;
    }

    public OpenSaasAddSubMerchantMerchantQuery setThirdparty_id(String thirdparty_id) {
        this.thirdparty_id = thirdparty_id;
        return this;
    }

    public Integer getUrl_type() {
        return url_type;
    }

    public OpenSaasAddSubMerchantMerchantQuery setUrl_type(Integer url_type) {
        this.url_type = url_type;
        return this;
    }

    public static OpenSaasAddSubMerchantQueryBuilder builder() {
        return new OpenSaasAddSubMerchantQueryBuilder();
    }

    public static final class OpenSaasAddSubMerchantQueryBuilder {
        private Integer prod_id;
        private Integer role;
        private String sub_merchant_id;
        private String thirdparty_id;
        private Integer url_type;
        private TransactionMerchantTokenTypeEnum transactionMerchantTokenType = TransactionMerchantTokenTypeEnum.CLIENT_TOKEN;
        private Integer tenantId;
        private String clientKey;

        private OpenSaasAddSubMerchantQueryBuilder() {
        }

        public OpenSaasAddSubMerchantQueryBuilder prodId(Integer prodId) {
            this.prod_id = prodId;
            return this;
        }

        public OpenSaasAddSubMerchantQueryBuilder role(Integer role) {
            this.role = role;
            return this;
        }

        public OpenSaasAddSubMerchantQueryBuilder subMerchantId(String subMerchantId) {
            this.sub_merchant_id = subMerchantId;
            return this;
        }

        public OpenSaasAddSubMerchantQueryBuilder thirdpartyId(String thirdpartyId) {
            this.thirdparty_id = thirdpartyId;
            return this;
        }

        public OpenSaasAddSubMerchantQueryBuilder urlType(Integer urlType) {
            this.url_type = urlType;
            return this;
        }

        public OpenSaasAddSubMerchantQueryBuilder transactionMerchantTokenType(TransactionMerchantTokenTypeEnum transactionMerchantTokenType) {
            this.transactionMerchantTokenType = transactionMerchantTokenType;
            return this;
        }

        public OpenSaasAddSubMerchantQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public OpenSaasAddSubMerchantQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public OpenSaasAddSubMerchantMerchantQuery build() {
            OpenSaasAddSubMerchantMerchantQuery openSaasAddSubMerchantQuery = new OpenSaasAddSubMerchantMerchantQuery();
            openSaasAddSubMerchantQuery.setProd_id(prod_id);
            openSaasAddSubMerchantQuery.setRole(role);
            openSaasAddSubMerchantQuery.setSub_merchant_id(sub_merchant_id);
            openSaasAddSubMerchantQuery.setThirdparty_id(thirdparty_id);
            openSaasAddSubMerchantQuery.setUrl_type(url_type);
            openSaasAddSubMerchantQuery.setTransactionMerchantTokenType(transactionMerchantTokenType);
            openSaasAddSubMerchantQuery.setTenantId(tenantId);
            openSaasAddSubMerchantQuery.setClientKey(clientKey);
            return openSaasAddSubMerchantQuery;
        }
    }
}
