package com.dyj.applet.domain.vo;

import com.dyj.common.domain.vo.BaseVo;

/**
 * @author danmo
 * @date 2024-04-29 16:29
 **/
public class SpuStockVo extends BaseVo {

    /**
     * 接入方SPU ID
     */
    private String spu_ext_id;

    public String getSpu_ext_id() {
        return spu_ext_id;
    }

    public void setSpu_ext_id(String spu_ext_id) {
        this.spu_ext_id = spu_ext_id;
    }
}
