package com.dyj.applet.domain.vo;

import com.dyj.common.domain.vo.BaseVo;

/**
 * @author danmo
 * @date 2024-05-27 18:43
 **/
public class CreateUserTaskVo extends BaseVo {

    private String task_id;

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }
}
