package com.dyj.applet.domain.query;

import com.dyj.applet.domain.AuditTemplateInfo;
import com.dyj.applet.domain.CooperationInfo;
import com.dyj.applet.domain.EmployeeInfo;
import com.dyj.common.domain.query.BaseQuery;

import java.util.List;
import java.util.Map;

/**
 * @author danmo
 * @date 2024-06-24 16:20
 **/
public class BindAwemeRelaionQuery extends BaseQuery {

    /**
     * 抖音号
     */
    private String aweme_id;

    /**
     * 绑定能力列表
     * video_self_mount：短视频自主挂载
     * live_self_mount：直播自主挂载
     * ma.im.life_im: 本地生活
     * imma.component.instant_messaging：私信组件
     * ma.jsapi.authorizePrivateMessage：主动授权私信组件
     */
    private List<String> capacity_list;

    /**
     * 绑定类型
     * brand：品牌号绑定
     * cooperation：合作号绑定
     * employee：员工号绑定
     */
    private String type;

    /**
     * 资质信息map，每个能力只需要上传一个资质信息map 的 key 为能力 capacity_keyvalue 为资质信息
     */
    private Map<String, AuditTemplateInfo> audit_template_info;

    /**
     * 抖音号和小程序是否是相同的主体，品牌号绑定时必传
     */
    private Boolean co_subject;

    /**
     * 合作号信息，合作号绑定时必传
     */
    private CooperationInfo cooperation_info;

    /**
     * 员工号信息，员工号绑定时必传
     */
    private EmployeeInfo employee_info;


    public static BindAwemeRelaionQueryBuilder builder() {
        return new BindAwemeRelaionQueryBuilder();
    }

    public static class BindAwemeRelaionQueryBuilder {
        /**
         * 抖音号
         */
        private String awemeId;

        /**
         * 绑定能力列表
         * video_self_mount：短视频自主挂载
         * live_self_mount：直播自主挂载
         * ma.im.life_im: 本地生活
         * imma.component.instant_messaging：私信组件
         * ma.jsapi.authorizePrivateMessage：主动授权私信组件
         */
        private List<String> capacityList;

        /**
         * 绑定类型
         * brand：品牌号绑定
         * cooperation：合作号绑定
         * employee：员工号绑定
         */
        private String type;

        /**
         * 资质信息map，每个能力只需要上传一个资质信息map 的 key 为能力 capacity_keyvalue 为资质信息
         */
        private Map<String, AuditTemplateInfo> auditTemplateInfo;

        /**
         * 抖音号和小程序是否是相同的主体，品牌号绑定时必传
         */
        private Boolean coSubject;

        /**
         * 合作号信息，合作号绑定时必传
         */
        private CooperationInfo cooperationInfo;

        /**
         * 员工号信息，员工号绑定时必传
         */
        private EmployeeInfo employeeInfo;

        private Integer tenantId;

        private String clientKey;

        public BindAwemeRelaionQueryBuilder awemeId(String awemeId) {
            this.awemeId = awemeId;
            return this;
        }

        public BindAwemeRelaionQueryBuilder capacityList(List<String> capacityList) {
            this.capacityList = capacityList;
            return this;
        }

        public BindAwemeRelaionQueryBuilder type(String type) {
            this.type = type;
            return this;
        }

        public BindAwemeRelaionQueryBuilder auditTemplateInfo(Map<String, AuditTemplateInfo> auditTemplateInfo) {
            this.auditTemplateInfo = auditTemplateInfo;
            return this;
        }

        public BindAwemeRelaionQueryBuilder coSubject(Boolean coSubject) {
            this.coSubject = coSubject;
            return this;
        }

        public BindAwemeRelaionQueryBuilder cooperationInfo(CooperationInfo cooperationInfo) {
            this.cooperationInfo = cooperationInfo;
            return this;
        }

        public BindAwemeRelaionQueryBuilder employeeInfo(EmployeeInfo employeeInfo) {
            this.employeeInfo = employeeInfo;
            return this;
        }

        public BindAwemeRelaionQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public BindAwemeRelaionQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public BindAwemeRelaionQuery build() {
            BindAwemeRelaionQuery bindAwemeRelaionQuery = new BindAwemeRelaionQuery();
            bindAwemeRelaionQuery.setAweme_id(awemeId);
            bindAwemeRelaionQuery.setCapacity_list(capacityList);
            bindAwemeRelaionQuery.setType(type);
            bindAwemeRelaionQuery.setAudit_template_info(auditTemplateInfo);
            bindAwemeRelaionQuery.setCo_subject(coSubject);
            bindAwemeRelaionQuery.setCooperation_info(cooperationInfo);
            bindAwemeRelaionQuery.setEmployee_info(employeeInfo);
            bindAwemeRelaionQuery.setTenantId(tenantId);
            bindAwemeRelaionQuery.setClientKey(clientKey);
            return bindAwemeRelaionQuery;
        }
    }

    public String getAweme_id() {
        return aweme_id;
    }

    public void setAweme_id(String aweme_id) {
        this.aweme_id = aweme_id;
    }

    public List<String> getCapacity_list() {
        return capacity_list;
    }

    public void setCapacity_list(List<String> capacity_list) {
        this.capacity_list = capacity_list;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, AuditTemplateInfo> getAudit_template_info() {
        return audit_template_info;
    }

    public void setAudit_template_info(Map<String, AuditTemplateInfo> audit_template_info) {
        this.audit_template_info = audit_template_info;
    }

    public Boolean getCo_subject() {
        return co_subject;
    }

    public void setCo_subject(Boolean co_subject) {
        this.co_subject = co_subject;
    }

    public CooperationInfo getCooperation_info() {
        return cooperation_info;
    }

    public void setCooperation_info(CooperationInfo cooperation_info) {
        this.cooperation_info = cooperation_info;
    }

    public EmployeeInfo getEmployee_info() {
        return employee_info;
    }

    public void setEmployee_info(EmployeeInfo employee_info) {
        this.employee_info = employee_info;
    }
}
