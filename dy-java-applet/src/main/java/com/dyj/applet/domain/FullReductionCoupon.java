package com.dyj.applet.domain;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-06 16:32
 **/
public class FullReductionCoupon {

    private List<FullReductionInfo> full_reduction_infos;

    public static FullReductionCouponBuilder builder() {
        return new FullReductionCouponBuilder();
    }

    public static class FullReductionCouponBuilder {
        private List<FullReductionInfo> fullReductionInfos;

        public FullReductionCouponBuilder fullReductionInfos(List<FullReductionInfo> fullReductionInfos) {
            this.fullReductionInfos = fullReductionInfos;
            return this;
        }

        public FullReductionCoupon build() {
            FullReductionCoupon fullReductionCoupon = new FullReductionCoupon();
            fullReductionCoupon.setFull_reduction_infos(fullReductionInfos);
            return fullReductionCoupon;
        }
    }


    public List<FullReductionInfo> getFull_reduction_infos() {
        return full_reduction_infos;
    }

    public void setFull_reduction_infos(List<FullReductionInfo> full_reduction_infos) {
        this.full_reduction_infos = full_reduction_infos;
    }
}
