package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.CommonPlan;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-13 16:06
 **/
public class CommonPlanListVo {

    /**
     * 总页数
     */
    private Integer page_count;

    /**
     * 总数
     */
    private Integer total;

    private List<CommonPlan> data;

    public Integer getPage_count() {
        return page_count;
    }

    public void setPage_count(Integer page_count) {
        this.page_count = page_count;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<CommonPlan> getData() {
        return data;
    }

    public void setData(List<CommonPlan> data) {
        this.data = data;
    }
}
