package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-06-26 17:29
 **/
public class AwemeBindTemplateInfo {

    /**
     * 是否可以使用该资质模版进行抖音号绑定
     */
    private Boolean can_apply;
    /**
     * 资质模版所属的类目id
     */
    private String category_id;
    /**
     * 资质模版所属的类目名称
     */
    private String category_name;
    /**
     * 资质模版 ID
     */
    private Long template_id;
    /**
     * 资质模版标题
     */
    private String title;
    /**
     * 不可使用的原因
     */
    private String reason;

    public Boolean getCan_apply() {
        return can_apply;
    }

    public void setCan_apply(Boolean can_apply) {
        this.can_apply = can_apply;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public Long getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(Long template_id) {
        this.template_id = template_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
