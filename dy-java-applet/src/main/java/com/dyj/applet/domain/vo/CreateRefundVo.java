package com.dyj.applet.domain.vo;

/**
 * 发起退款返回值
 */
public class CreateRefundVo {

    /**
     * 退款审核的最后期限，13位unix时间戳，精度：毫秒
     */
    private Long refund_audit_deadline;
    /**
     * 抖音开放平台交易系统内部退款单号
     */
    private String refund_id;

    public Long getRefund_audit_deadline() {
        return refund_audit_deadline;
    }

    public CreateRefundVo setRefund_audit_deadline(Long refund_audit_deadline) {
        this.refund_audit_deadline = refund_audit_deadline;
        return this;
    }

    public String getRefund_id() {
        return refund_id;
    }

    public CreateRefundVo setRefund_id(String refund_id) {
        this.refund_id = refund_id;
        return this;
    }
}
