package com.dyj.applet.domain.vo;

/**
 * @author danmo
 * @date 2024-06-26 17:21
 **/
public class AwemeRelationBindQrcodeVo {

    /**
     * 二维码url字符串，需开发者自行构造二维码图片
     */
    private String qrcode_parse_content;
    /**
     * 二维码图片url，可直接访问展示二维码图片。有效期为 24 小时
     */
    private String qrcode_url;

    public String getQrcode_parse_content() {
        return qrcode_parse_content;
    }

    public void setQrcode_parse_content(String qrcode_parse_content) {
        this.qrcode_parse_content = qrcode_parse_content;
    }

    public String getQrcode_url() {
        return qrcode_url;
    }

    public void setQrcode_url(String qrcode_url) {
        this.qrcode_url = qrcode_url;
    }
}
