package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-04-29 16:40
 **/
public class SpuHighlight {

    /**
     * 介绍，字符串长度<=5
     */
    private String content;

    /**
     * 优先级，数字越小优先级越高
     */
    private Integer priority;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
