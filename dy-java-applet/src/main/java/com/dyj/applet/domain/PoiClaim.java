package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-06 16:01
 **/
public class PoiClaim {

    /**
     * 	地址详情
     */
    private String address;
    /**
     * poi名称
     */
    private String name;
    /**
     * 	poi的Id
     */
    private String poi_id;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPoi_id() {
        return poi_id;
    }

    public void setPoi_id(String poi_id) {
        this.poi_id = poi_id;
    }
}
