package com.dyj.applet.domain.query;

import com.dyj.applet.domain.LivePlanProduct;
import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-13 16:25
 **/
public class SaveLivePlanQuery extends BaseQuery {

    private List<String> douyin_id_list;

    private String merchant_phone;

    private String plan_id;

    private String plan_name;

    /**
     * 计划选定商品id List
     */
    private List<LivePlanProduct> product_list;

    public static SaveLivePlanQueryBuilder builder() {
        return new SaveLivePlanQueryBuilder();
    }

    public static class SaveLivePlanQueryBuilder {
        private List<String> douyinIdList;

        private String merchantPhone;

        private String planId;

        private String planName;

        private List<LivePlanProduct> productList;

        private Integer tenantId;

        private String clientKey;

        public SaveLivePlanQueryBuilder douyinIdList(List<String> douyinIdList) {
            this.douyinIdList = douyinIdList;
            return this;
        }

        public SaveLivePlanQueryBuilder merchantPhone(String merchantPhone) {
            this.merchantPhone = merchantPhone;
            return this;
        }

        public SaveLivePlanQueryBuilder planId(String planId) {
            this.planId = planId;
            return this;
        }

        public SaveLivePlanQueryBuilder planName(String planName) {
            this.planName = planName;
            return this;
        }

        public SaveLivePlanQueryBuilder productList(List<LivePlanProduct> productList) {
            this.productList = productList;
            return this;
        }

        public SaveLivePlanQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public SaveLivePlanQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public SaveLivePlanQuery build() {
            SaveLivePlanQuery saveLivePlanQuery = new SaveLivePlanQuery();
            saveLivePlanQuery.setDouyin_id_list(douyinIdList);
            saveLivePlanQuery.setMerchant_phone(merchantPhone);
            saveLivePlanQuery.setPlan_id(planId);
            saveLivePlanQuery.setPlan_name(planName);
            saveLivePlanQuery.setProduct_list(productList);
            saveLivePlanQuery.setTenantId(tenantId);
            saveLivePlanQuery.setClientKey(clientKey);
            return saveLivePlanQuery;
        }
    }


    public List<String> getDouyin_id_list() {
        return douyin_id_list;
    }

    public void setDouyin_id_list(List<String> douyin_id_list) {
        this.douyin_id_list = douyin_id_list;
    }

    public String getMerchant_phone() {
        return merchant_phone;
    }

    public void setMerchant_phone(String merchant_phone) {
        this.merchant_phone = merchant_phone;
    }

    public String getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(String plan_id) {
        this.plan_id = plan_id;
    }

    public String getPlan_name() {
        return plan_name;
    }

    public void setPlan_name(String plan_name) {
        this.plan_name = plan_name;
    }

    public List<LivePlanProduct> getProduct_list() {
        return product_list;
    }

    public void setProduct_list(List<LivePlanProduct> product_list) {
        this.product_list = product_list;
    }
}
