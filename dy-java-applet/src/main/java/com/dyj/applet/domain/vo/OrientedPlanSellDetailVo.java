package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.OrientedPlanSellDetail;

import java.util.Map;

public class OrientedPlanSellDetailVo {

    private Map<String, OrientedPlanSellDetail> data;

    private String date;

    public Map<String, OrientedPlanSellDetail> getData() {
        return data;
    }

    public void setData(Map<String, OrientedPlanSellDetail> data) {
        this.data = data;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
